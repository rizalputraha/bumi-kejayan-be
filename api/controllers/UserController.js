/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/library/LibraryController');
const AuthLibrary = require('../controllers/library/AuthLibraryController');
 
const UserController = {
    listUser : async (req, res) => {
        await LibraryController.response(req, res, async (body) =>{
            const {filter} = req.query;
            let query;
            if (filter == 'all' || !filter) {
                query = await User.find({select:['nama', 'email', 'role', 'created_at']}).sort('created_at ASC');   
            } else if (filter == 'active') {
                query = await User.find({select:['nama', 'email', 'role', 'created_at'],where: {status: 0}}).sort('created_at ASC');   
            } else if (filter == 'banned') {
                query = await User.find({select:['nama', 'email', 'role', 'created_at'],where: {status: 1}}).sort('created_at ASC');  
            } else if (filter == 'notactive') {
                query = await User.find({select:['nama', 'email', 'role', 'created_at'],where: {status: 2}}).sort('created_at ASC');  
            } else {
                throw new Error('Data Not Found!');
            }
            const result = LibraryController.addNomerList(query);
            return result;
        });
    },

    listUserPaging: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {filter} = req.query;
            const {page, perPage} = req.params;
            let query;
            let total;
            if (filter == 'all' || !filter) {
                query = await User.find({select:['nama', 'email', 'role', 'created_at'], limit: perPage, skip: page * perPage - perPage}).sort('created_at ASC');
                total = await User.count();
            } else if (filter == 'active') {
                query = await User.find({select:['nama', 'email', 'role', 'created_at'],where: {status: 0}, limit: perPage, skip: page * perPage - perPage}).sort('created_at ASC');   
                total = await User.count({status: 0});
            } else if (filter == 'banned') {
                query = await User.find({select:['nama', 'email', 'role', 'created_at'],where: {status: 1}, limit: perPage, skip: page * perPage - perPage}).sort('created_at ASC');  
                total = await User.count({status: 1});
            } else if (filter == 'notactive') {
                query = await User.find({select:['nama', 'email', 'role', 'created_at'],where: {status: 2}, limit: perPage, skip: page * perPage - perPage}).sort('created_at ASC');  
                total = await User.count({status: 2});
            } else {
                throw new Error('Data Not Found!');
            }
            const result = LibraryController.addNomerList(query);
            return {data:result, total};
        })
    },

    detailUser: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {id} = req.params;
            const result = await User.findOne({select:['nama', 'email', 'role', 'created_at'], where: {id: id}});
            return result;
        });
    },

    editUser: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {id} = req.params;
            const result = await UserController.updateProcess(id, req.body);
            return result;
        });
    },

    updateProcess : async (id, body) => {
        let hashed = "";
        let obj;
        if (body.password != undefined){
            hashed = AuthLibrary.createSalt({email: body.email}, body.password);
            obj = {...body, password: hashed};
        } else {
            obj = {...body};
        }
        
        const result = await User.updateOne({id: id}).set(obj);
        return "Sukses update user";
    },
}

module.exports = UserController;


/**
 * LibraryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = {
    response: async (req, res, callback) => {
        let jres = {
            error: 0,
            data: [],
            message: '',
            stack: {},
            errorName: ''
        }
        try {
            jres.data = await callback(req.body)
        } catch (error) {
            jres.error = error.name == "UsageError" ? 400 : 500
            jres.message = error.name == "UsageError" ? "Bad Request!" : error.message
            jres.stack = error.stack
            jres.errorName = error.name
        }
        res.json(jres)
    },

    addNomerList: (query) => {
        for (let i = 0; i < query.length; i++) {
            query[i].nomer = i+1;
        }
        return query;
    }
}

module.exports = LibraryController;


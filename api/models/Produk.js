/**
 * Produk.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "t_produk",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_produk',
      autoIncrement: true
    },
    
    kd_produk: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    nama: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    unit: {
      type: 'string',
      required: true,
      maxLength: 20
    },

    harga_jual: {
      type: 'number',
    },

    kategori: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    status: {
      type: 'number',
    },

    created_at: {
      type: 'string',
      autoCreatedAt: true,
    },
    
    updated_at: {
      type: 'string',
      autoUpdatedAt: true,
    },
  },
};


/**
 * Pelanggan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "t_pelanggan",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_pelanggan',
      autoIncrement: true
    },
    
    kd_pelanggan: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    nama: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    nama_perusahaan: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    alamat: {
      type: 'string',
      required: true,
      maxLength: 255
    },

    email: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    notelp: {
      type: 'string',
      required: true,
      maxLength: 16
    },

    status: {
      type: 'number',
    },

    created_at: {
      type: 'string',
      autoCreatedAt: true,
    },
    
    updated_at: {
      type: 'string',
      autoUpdatedAt: true,
    },
  },
};


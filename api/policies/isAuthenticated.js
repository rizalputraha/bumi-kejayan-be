var jwt = require('jsonwebtoken');
let env = require('../../env');

module.exports=function(req, res, next) {
    let signerOption = {
        issuer:'mfi',
        expiresIn: '1d',
    }
    const token = req.headers['npc-token'] || false;
    const verify = (tok) => {
        try {
            return jwt.verify(tok, env.privatKey, signerOption)
        } catch (error) {
            // console.log('VERIFY ERR ', error.message, error.name);
            res.status(200);
            return res.json({error: 401,
                data: [],
                message: `${error.message} or Token Invalid!`,
                stack: {},
                errorName: ''});
        }
    }
    const decode = (tok) => {
        return jwt.decode(tok, {complete: true});
    }

    if (token && verify(token)) {
        res.locals.udata = decode(token);
        // console.log(res.locals, {token});
        next();
    } else {
        res.status(200)
        return res.json({error: 401,
            data: [],
            message: 'Token Invalid! or Session Expired',
            stack: {},
            errorName: ''});
    }

    
}
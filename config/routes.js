/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: '404' },


  //  ╔═╗╔═╗╦
  //  ╠═╣╠═╝║
  //  ╩ ╩╩  ╩

  'POST /api/v1/user/login': 'auth.LoginController.login',
  'POST /api/v1/user/refresh_token': 'auth.LoginController.refreshToken',
  '/api/v1/user/logout': 'auth.LoginController.logout',
  'POST /api/v1/user/signup': 'auth.SignupController.register',

  // USER WEB
  'GET /api/v1/user/list': 'UserController.listUser',
  'GET /api/v1/user/list/paging/:page/:perPage': 'UserController.listUserPaging',
  'GET /api/v1/user/detail/:id': 'UserController.detailUser',
  'POST /api/v1/user/edit/:id': 'UserController.editUser',

  // TIMBANGAN WEB
  'POST /api/v1/timbangan/create': 'TimbanganController.create',
  'GET /api/v1/timbangan/read': 'TimbanganController.read',
  'GET /api/v1/timbangan/read/paging/:page/:perPage': 'TimbanganController.readPaging',
  'POST /api/v1/timbangan/edit/:id': 'TimbanganController.edit',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
